package ir.ac.kntu;

public class Cat extends Animal implements Voiced {



    public Cat(Integer age, double moveUp, double moveDown, double moveRight, double moveLeft) {
        super(age, moveUp, moveDown, moveRight, moveLeft);
        // TODO Auto-generated constructor stub
    }

    public void moveUp(Double amount) {
       double move=amount-getMoveDown();
       setMoveUp(move);
        System.out.println(getClass().getName()+"is movingUp"+getMoveUp());

    }

    public void moveDown(Double amount) {
        double move=amount-getMoveUp();
        setMoveDown(move);
        System.out.println(getClass().getName()+"is movingDown"+getMoveDown());

    }

    public void moveRight(Double amount) {
        double moveR=amount-getMoveLeft();
        setMoveRight(moveR);
        System.out.println(getClass().getName()+"is movingRight"+getMoveRight());

    }

    public void moveLeft(Double amount) {
             
        double moveR=amount-getMoveRight();
        setMoveLeft(moveR);
        System.out.println(getClass().getName()+"is movingLeft"+getMoveLeft());

    }

    public void makeSound() {
        System.out.println(getClass().getName()+"makes some noise");

    }

    @Override
    void eatFood() {
        System.out.println(getClass().getName()+"is eating food");

    }

   

    

   
}
