package ir.ac.kntu;

public abstract class Bird extends Animal {
    protected Double beakLength;

    public Bird(Integer age, double moveUp, double moveDown, double moveRight, double moveLeft, Double beakLength) {
        super(age, moveUp, moveDown, moveRight, moveLeft);
        this.beakLength = beakLength;
    }

   
    // TODO: Complete this class
}
